#include "PolygoneRegulier.hpp"
#include <math.h>
#include <iostream>

PolygoneRegulier::PolygoneRegulier(const Couleur &couleur, const Point &centre, int rayon, int nbcotes):
    FigureGeometrique(couleur), _nbPoints(nbcotes){
    int cpt=0;
    for(int i=0;i<nbcotes;i++){
        _points.push_back(Point{rayon*cos((2*M_PI)/nbcotes*i) + centre._x,rayon*sin((2*M_PI)/nbcotes*i) + centre._y});
        cpt++;
    }
}

void PolygoneRegulier::afficher() const{
    std::cout<<"PolygoneRegulier "<<_couleur._r<<"_"<<_couleur._g<<"_"<<_couleur._b;
    for(int i=0;i<_nbPoints;i++){
        std::cout<<" "<<_points[i]._x<<"_"<<_points[i]._y;
    }
    std::cout<<std::endl;
}


int PolygoneRegulier::getNbPoints() const{
    return _nbPoints;
}

const Point& PolygoneRegulier::getPoint(int indice) const{
    return _points[indice];
}

PolygoneRegulier::~PolygoneRegulier(){
    //delete[] _points;
}
