#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"
#include <vector>

int main(){
    Point p0{6,7};
    Point p1{7,3};
    Couleur c{1.0,0.6,0.1};
    Ligne testL(c,p0,p1);

    Couleur coul{1.0,0.5,0.3};
    Point p{100,200};
    PolygoneRegulier testPR(coul,p,50,5);

    std::vector<FigureGeometrique*> V;
    V.push_back(&testPR);
    V.push_back(&testL);
    V.push_back(&testPR);
    for(unsigned i=0;i<V.size();i++){
        V[i]->afficher();
    }
    return 0;
}
