#ifndef PG_
#define PG_
#include "FigureGeometrique.hpp"
#include "Couleur.hpp"
#include "Point.hpp"
#include <vector>

class PolygoneRegulier : public FigureGeometrique{
private:
    int _nbPoints;
    std::vector<Point> _points;
public:
    PolygoneRegulier(const Couleur& couleur, const Point& centre, int rayon, int nbcotes);
    ~PolygoneRegulier();
    void afficher() const override;
    int getNbPoints() const;
    const Point& getPoint(int indice) const;
};

#endif
