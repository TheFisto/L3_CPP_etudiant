#ifndef FG_
#define FG_

#include "Couleur.hpp"

class FigureGeometrique{
    protected:  //seul les classes héritant de celle ci on accès à ces attributs
        Couleur _couleur;
    public:
        //methodes
        FigureGeometrique(const Couleur& couleur);
        const Couleur& getCouleur() const;
        //methodes virtuelles
        virtual void afficher() const =0;
};

#endif
