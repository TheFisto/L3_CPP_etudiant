#include "../src/Point.hpp"
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupPoint){};
TEST(GroupPoint, test_crea_Point){
    Point testP{4,8};
    CHECK_EQUAL(testP._x,4);
    CHECK_EQUAL(testP._y,8);
}
