#include "../src/Couleur.hpp"
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupCouleur){};
TEST(GroupCouleur, test_crea_Couleur){
    Couleur test{4.25,8,7};
    CHECK_EQUAL(test._r,4.25);
    CHECK_EQUAL(test._g,8);
    CHECK_EQUAL(test._b,7);
}
