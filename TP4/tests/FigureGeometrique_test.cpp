#include "../src/FigureGeometrique.hpp"
#include "../src/Couleur.hpp"
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupFigureGeometrique){};
TEST(GroupFigureGeometrique, test_crea_FigureGeometrique){
    Couleur testC{1.0,2.0,3.0};
    FigureGeometrique testFG(testC);
}

TEST(GroupFigureGeometrique, test_getCouleur_FigureGeometrique){
    Couleur testC{1.0,2.0,3.0};
    FigureGeometrique testFG(testC);
    CHECK_EQUAL(testFG.getCouleur()._r,1.0)
    CHECK_EQUAL(testFG.getCouleur()._g,2.0)
    CHECK_EQUAL(testFG.getCouleur()._b,3.0)
}
