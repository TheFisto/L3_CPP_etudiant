#include "../src/Ligne.hpp"
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupLigne){};
Point p0{6,7};
Point p1{7,3};
Couleur c{1.0,0.6,0.1};
TEST(GroupLigne, test_crea_Ligne){
    Ligne testL(c,p0,p1);
}

Ligne testL(c,p0,p1);

TEST(GroupLigne, test_getP0_Ligne){
    CHECK_EQUAL(testL.getP0()._x,p0._x);
    CHECK_EQUAL(testL.getP0()._y,p0._y);
}

TEST(GroupLigne, test_getP1_Ligne){
    CHECK_EQUAL(testL.getP1()._x,p1._x);
    CHECK_EQUAL(testL.getP1()._y,p1._y);
}
