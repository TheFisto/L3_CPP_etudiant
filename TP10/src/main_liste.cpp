 #include <iostream>

#include "Liste.hpp"
#include "ListeGenerique.hpp"
#include "Persone.hpp"

int main() {

    ListeGenerique<Personne> l1;
    l1.push_front(Personne{"john",37});
    l1.push_front(Personne{"Sarah",13});
    l1.push_front(Personne{"Arnold",23});
    std::cout << l1 << std::endl;

    l1.clear();
    if(l1.empty())
        std::cout<<"yeet"<<std::endl;
    l1.push_front(Personne{"john",37});
    l1.push_front(Personne{"Sarah",13});
    for(auto it : l1)
        std::cout<<it<<std::endl;
    std::cout<<l1.front()<<std::endl;
    return 0;
}

