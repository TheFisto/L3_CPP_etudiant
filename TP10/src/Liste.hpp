
#ifndef LISTE_HPP_
#define LISTE_HPP_

#include <cassert>
#include <ostream>
#include <iostream>

// liste d'entiers avec itérateur
class Liste {
    private:
        struct Noeud {
            int _valeur;
            Noeud* _ptrNoeudSuivant;
        };

    public:
        class iterator {
            private:
                Noeud* _ptrNoeudCourant;
            public:
                iterator(Noeud* ptrNoeudCourant) : _ptrNoeudCourant(ptrNoeudCourant){

                }

                const iterator & operator++() {
                    _ptrNoeudCourant = _ptrNoeudCourant->_ptrNoeudSuivant;
                    return *this;
                }

                int& operator*() const {
                    return _ptrNoeudCourant->_valeur;
                }

                bool operator!=(const iterator & iter) const {
                    return _ptrNoeudCourant != iter._ptrNoeudCourant;
                }

                friend Liste; 
        };

    private:
        Noeud* _ptrTete;

    public:
        Liste() : _ptrTete(nullptr){

        }

        ~Liste(){
            clear();
        }

        void push_front(int val) {
            _ptrTete = new Noeud{val, _ptrTete};
        }

        int& front() const {
            return _ptrTete->_valeur;
        }

        void clear() {
            Noeud * precedent = _ptrTete;
            while (_ptrTete) {
                _ptrTete = _ptrTete->_ptrNoeudSuivant;
                delete precedent;
                precedent = _ptrTete;
            }
        }

        bool empty() const {
            return !_ptrTete;
        }

        iterator begin() const {
            return iterator(_ptrTete);
        }

        iterator end() const {
            return iterator(nullptr);
        }

};

std::ostream& operator<<(std::ostream& os, const Liste& liste) {
    for(int it : liste){
        os << it << " ";
    }
    return os;
}

#endif

