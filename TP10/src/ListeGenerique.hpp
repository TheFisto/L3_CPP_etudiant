
#ifndef LISTEGENERIQUE_
#define LISTEGENERIQUE_

#include <cassert>
#include <ostream>
#include <iostream>

template <typename T> class ListeGenerique {
    private:
        struct Noeud {
            T _valeur;
            Noeud* _ptrNoeudSuivant;
        };

    public:
        class iterator {
            private:
                Noeud* _ptrNoeudCourant;
            public:
                iterator(Noeud* ptrNoeudCourant) : _ptrNoeudCourant(ptrNoeudCourant){

                }

                const iterator & operator++() {
                    _ptrNoeudCourant = _ptrNoeudCourant->_ptrNoeudSuivant;
                    return *this;
                }

                T& operator*() const {
                    return _ptrNoeudCourant->_valeur;
                }

                bool operator!=(const iterator & iter) const {
                    return _ptrNoeudCourant != iter._ptrNoeudCourant;
                }

                friend ListeGenerique;
        };

    private:
        Noeud* _ptrTete;

    public:
        ListeGenerique() : _ptrTete(nullptr){

        }

        ~ListeGenerique(){
            clear();
        }

        void push_front(T val) {
            _ptrTete = new Noeud{val, _ptrTete};
        }

        T& front() const {
            return _ptrTete->_valeur;
        }

        void clear() {
            Noeud * precedent = _ptrTete;
            while (_ptrTete) {
                _ptrTete = _ptrTete->_ptrNoeudSuivant;
                delete precedent;
                precedent = _ptrTete;
            }
        }

        bool empty() const {
            return !_ptrTete;
        }

        iterator begin() const {
            return iterator(_ptrTete);
        }

        iterator end() const {
            return iterator(nullptr);
        }

};

template <typename T> std::ostream& operator<<(std::ostream& os, const ListeGenerique<T>& liste) {
    for(T it : liste){
        os << it << " ";
    }
    return os;
}

#endif

