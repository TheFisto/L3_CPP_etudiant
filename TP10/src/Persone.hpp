#ifndef PERSONNE_
#define PERSONNE_

#include <string>
#include <ostream>

struct Personne{
    std::string _nom;
    int _age;
};

std::ostream& operator<<(std::ostream & os, const Personne& personne){
    os << personne._nom << " " << personne._age;
    return os;
}

#endif
