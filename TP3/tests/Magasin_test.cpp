#include "../src/Magasin.hpp"
#include <string>
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupMagasin){};
TEST(GroupMagasin, test_crea_magasin){
    Magasin testMagasin;
}

//CLIENTS

TEST(GroupMagasin, test_nb_clients){
    Magasin testMagasin;
    CHECK_EQUAL(testMagasin.nbClient(),0);
}

TEST(GroupMagasin, test_add_clients){
    Magasin testMagasin;
    testMagasin.ajouterClient("Steve");
    CHECK_EQUAL(testMagasin.nbClient(),1);
    for(int i=0;i<10;i++){
        testMagasin.ajouterClient("clientTestN"+std::to_string(i+1));
    }
    CHECK_EQUAL(testMagasin.nbClient(),11);
    testMagasin.ajouterClient("");
    CHECK_EQUAL(testMagasin.nbClient(),12);
}

TEST(GroupMagasin, test_suppr_clients){
    Magasin testMagasin;
    for(int i=0;i<4;i++){
        testMagasin.ajouterClient("Client"+std::to_string(i+1));
    }
    testMagasin.supprimerClient(2);
    CHECK_EQUAL(testMagasin.nbClient(),3);
    CHECK_THROWS(std::string, testMagasin.supprimerClient(-1));
}

//PRODUITS

TEST(GroupMagasin, test_nb_produits){
    Magasin testMagasin;
    CHECK_EQUAL(testMagasin.nbProduits(),0);
}

TEST(GroupMagasin, test_add_produits){
    Magasin testMagasin;
    testMagasin.ajouterProduit("Steve");
    CHECK_EQUAL(testMagasin.nbProduits(),1);
    for(int i=0;i<10;i++){
        testMagasin.ajouterProduit("produitTestN"+std::to_string(i+1));
    }
    CHECK_EQUAL(testMagasin.nbProduits(),11);
    testMagasin.ajouterProduit("");
    CHECK_EQUAL(testMagasin.nbProduits(),12);
}

TEST(GroupMagasin, test_suppr_produits){
    Magasin testMagasin;
    for(int i=0;i<4;i++){
        testMagasin.ajouterProduit("Produit"+std::to_string(i+1));
    }
    testMagasin.supprimerProduit(2);
    CHECK_EQUAL(testMagasin.nbProduits(),3);
}
