#include "../src/Client.hpp"
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupClient){};
TEST(GroupClient, test_crea_client){
    Client testClient(0,"Steve");
}

TEST(GroupClient, test_getId_client){
    Client testClient1(0,"Steve");
    Client testClient2(-1,"");

    CHECK_EQUAL(testClient1.getId(),0);
    CHECK_EQUAL(testClient2.getId(),-1);
}

TEST(GroupClient, test_getNom_client){
    Client testClient1(0,"Steve");
    Client testClient2(-1,"");

    CHECK_EQUAL(testClient1.getNom(),"Steve");
    CHECK_EQUAL(testClient2.getNom(),"");
}
