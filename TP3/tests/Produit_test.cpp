#include "../src/Produit.hpp"
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupProduit){};
TEST(GroupProduit, test_crea_Produit){
    Produit testProduit(0,"description");//TODO -- trouver moyen de tester les constructeurs
}

Produit testProduitGlobal1(1,"description");
Produit testProduitGlobal2(-1,"");

TEST(GroupProduit, test_getId_produit){
    CHECK_EQUAL(testProduitGlobal1.getId(),1);
    CHECK_EQUAL(testProduitGlobal2.getId(),-1);
}

TEST(GroupProduit, test_getDescription_produit){
    CHECK_EQUAL(testProduitGlobal1.getDescription(),"description");
    CHECK_EQUAL(testProduitGlobal2.getDescription(),"");
}
