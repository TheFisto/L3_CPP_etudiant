#include "../src/Location.hpp"
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupLocation){};
TEST(GroupLocation, test_crea_location){
    Location testLocation1{0,2};
    CHECK(testLocation1._idClient==0 && testLocation1._idProduit==2);
    Location testLocation2{-1,2};
    CHECK(testLocation2._idClient==-1 && testLocation2._idProduit==2);
    Location testLocation3{0,-1};
    CHECK(testLocation3._idClient==0 && testLocation3._idProduit==-1);
    Location testLocation4{-1,-1};
    CHECK(testLocation4._idClient==-1 && testLocation4._idProduit==-1);
}
