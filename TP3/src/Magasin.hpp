#ifndef MAGASIN_
#define MAGASIN_

#include "Client.hpp"
#include "Produit.hpp"
#include "Location.hpp"
#include <iostream>
#include <vector>

class Magasin{
    //attributs
    private:
        std::vector<Client> _clients;
        std::vector<Produit> _produits;
        std::vector<Location> _locations;
        int _idCourantClient;
        int _idCourantProduit;
    //methodes
    public:
        Magasin();
        //clients
        int nbClient() const;
        void ajouterClient(const std::string & nom);
        void afficherCLients() const;
        void supprimerClient(int idClient);
        //produits
        int nbProduits() const;
        void ajouterProduit(const std::string & nom);
        void afficherProduits() const;
        void supprimerProduit(int idProduit);
};

#endif
