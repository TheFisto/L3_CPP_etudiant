#include "Magasin.hpp"
#include "Client.hpp"
#include "Produit.hpp"
#include <vector>

Magasin::Magasin() : _idCourantClient(0), _idCourantProduit(0) {}

//CLIENTS

int Magasin::nbClient() const {return _clients.size();}

void Magasin::ajouterClient(const std::__cxx11::string &nom){
    _clients.push_back(Client(_idCourantClient++,nom));
}

void Magasin::afficherCLients() const {
    for(int i=0;i<nbClient();i++){
        _clients[i].afficher();
    }
}

void Magasin::supprimerClient(int idClient){
    for(int i=0;i<nbClient();i++){
        if(_clients[i].getId()==idClient){
            if(i+1!=nbClient()){
                std::swap(_clients[i],_clients[nbClient()-1]);
            }
            _clients.pop_back();
        }
    }
    throw std::string("erreur:ce client n'existe pas");
}

//PRODUITS

int Magasin::nbProduits() const {return _produits.size();}

void Magasin::ajouterProduit(const std::__cxx11::string &nom){
    _produits.push_back(Produit(_idCourantProduit++,nom));
}

void Magasin::afficherProduits() const {
    for(int i=0;i<nbProduits();i++){
        _produits[i].afficherProduit();
    }
}

void Magasin::supprimerProduit(int idProduit){
    for(int i=0;i<nbProduits();i++){
        if(_produits[i].getId()==idProduit){
            if(i+1!=nbProduits()){
                std::swap(_produits[i],_produits[nbProduits()-1]);
            }
            _produits.pop_back();
        }
    }
    throw std::string("erreur:ce produit n'existe pas");
}
