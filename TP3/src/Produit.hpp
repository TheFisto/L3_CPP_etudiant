#ifndef PRODUIT_
#define PRODUIT_

#include <iostream>

class Produit{
    //attributs
    private:
        int _id;
        std::string _description;
    //methodes
    public:
        Produit(int id, const std::string & description);
        int getId() const;
        const std::string & getDescription() const;
        void afficherProduit() const;
};

#endif
