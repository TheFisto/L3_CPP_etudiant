#ifndef LOCATION_
#define LOCATION_

#include <iostream>

struct Location{
    //attributs
    int _idClient;
    int _idProduit;

    //methodes
    void afficher() const{
        std::cout<<"Location ("<<_idClient<<", "<<_idProduit<<")"<<std::endl;
    }
};

#endif
