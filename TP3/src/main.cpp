#include <iostream>
#include "Location.hpp"
#include "Client.hpp"

int main() {
    Location testLocation{0,2};
    testLocation.afficher();
    Client testClient(42,"toto");
    testClient.afficher();
    return 0;
}

