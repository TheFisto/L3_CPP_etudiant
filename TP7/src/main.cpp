#include "Image.hpp"

#include <iostream>
#include <gtkmm.h>

int main(int argc, char** argv) {
    //creer image
    Image test(512,512);
    remplir(test);
    ecrirePnm(bordure(test,150,20),"out.pnm");

    Gtk::Main kit(argc, argv);
    //fenêtre
    Gtk::Window window;
    window.set_title("visionneur");
    window.set_size_request(512,512);
    window.set_resizable(false);
    window.set_border_width(3);
    //contenu
    Gtk::Image img("out.pnm");
    window.add(img);
    window.show_all();
    //affichage
    kit.run(window);
    return 0;
}

