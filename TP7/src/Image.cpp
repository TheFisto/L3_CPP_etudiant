#include "Image.hpp"
#include <fstream>
#include <math.h>
#include <iostream>

Image::Image(int largeur, int hauteur) : _largeur(largeur), _hauteur(hauteur){
    _pixels = new int[largeur*hauteur];
    for(int i=0;i<largeur*hauteur;i++)
        _pixels[i]=0;
}

Image::Image(const Image &img) : _largeur(img.getLargeur()), _hauteur(img.getHauteur()){
    _pixels = new int[_largeur*_hauteur];
    for(int i=0;i<_largeur;i++){
        for(int j=0;j<_hauteur;j++)
            _pixels[i+j*_largeur]=img.getPixel(i,j);
    }
}

Image::~Image(){
    delete [] _pixels;
}

int Image::getLargeur() const {return _largeur;}
int Image::getHauteur() const {return _hauteur;}
/*
int Image::getPixel(int i, int j) const{
    if(i >= _largeur || j>=_hauteur || i<0 || i<0)
        throw std::string("ERREUR: pixel hors de l'image.");
    return _pixels[i + j*_largeur];
}
*/
int& Image::getPixel(int i, int j) const{
    if(i >= _largeur || j>=_hauteur || i<0 || i<0)
        throw std::string("ERREUR: pixel hors de l'image.");
    return _pixels[i + j*_largeur];
}

void Image::setPixel(int i, int j, int couleur){
    if(i >= _largeur || j>=_hauteur || i<0 || i<0)
        throw std::string("ERREUR: pixel hors de l'image.");
    if(couleur<0)
        couleur = 0;
    else if(couleur > 255)
        couleur = 255;
    _pixels[i + j*_largeur] = couleur;
}

void ecrirePnm(const Image &img, const std::string &nomFichier){
    std::ofstream ofs(nomFichier);
    ofs<<"P2\n"<<img.getLargeur()<<" "<<img.getHauteur()<<"\n255\n";
    for(int i=0;i<img.getHauteur();i++){
        for(int j=0;j<img.getLargeur();j++){
            ofs<<img.getPixel(j,i);
            if(j!=img.getHauteur()-1)
                ofs<<" ";
        }
        if(i!=img.getLargeur())
            ofs<<"\n";
    }
}

void remplir(Image &img){
    for(int i=0;i<img.getLargeur();i++){
        for(int j=0;j<img.getHauteur();j++){
            img.setPixel(i,j,154*(1+cos((2*M_PI*i)/img.getLargeur()*3)));
        }
    }
}

Image bordure(const Image &img, int couleur, int epaisseur){
    Image ret(img);
    for(int i=0;i<epaisseur;i++){
        //haut
        for(int j=0;j<ret.getLargeur();j++)
            ret.setPixel(j,i,couleur);
        //bas
        for(int j=0;j<ret.getLargeur();j++)
            ret.setPixel(j,ret.getHauteur()-1-i,couleur);
        //gauche
        for(int j=0;j<ret.getHauteur();j++)
            ret.setPixel(i,j,couleur);
        //droite
        for(int j=0;j<ret.getHauteur();j++)
            ret.setPixel(ret.getLargeur()-1-i,j,couleur);
    }
    return ret;
}
