#ifndef IMAGE_
#define IMAGE_

#include <string>

class Image{
    private:
        //attributs
        int _largeur,
            _hauteur;
        int * _pixels;
    public:
        //constructeur/destructeur
        Image(int largeur, int hauteur);
        Image(const Image & img);
        ~Image();
        //get/set
        int getLargeur() const;
        int getHauteur() const;
        //int getPixel(int i,int j) const;
        void setPixel(int i, int j, int couleur);
        int& getPixel(int i, int j) const;
        //methodes
};

//fonctions
void ecrirePnm(const Image& img, const std::string & nomFichier);
void remplir(Image &img);
Image bordure(const Image & img, int couleur, int epaisseur);

#endif
