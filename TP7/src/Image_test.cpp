#include "Image.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupImage) { };

TEST(GroupImage, Constructeur)  {
    Image im(256,512);
    CHECK_EQUAL(im.getLargeur(), 256);
    CHECK_EQUAL(im.getHauteur(), 512);
}


TEST(GroupImage, getSetPixel1){
    Image testIm(256,256);
    CHECK_EQUAL(testIm.getPixel(2,2), 0);
}

TEST(GroupImage, getSetPixel2){
    Image testIm(256,256);
    try{
        testIm.getPixel(-2,10);
        FAIL("exception non levee");
    }
    catch(const std::string& s){
        CHECK_EQUAL(s,"ERREUR: pixel hors de l'image.");
    }
}

TEST(GroupImage, getSetPixel3){
    Image testIm(256,256);
    testIm.setPixel(2,2,12);
    CHECK_EQUAL(testIm.getPixel(2,2),12);
}

TEST(GroupImage, getSetPixel4){
    Image testIm(256,256);
    try{
        testIm.setPixel(-2,10,1);
        FAIL("exception non levee");
    }
    catch(const std::string& s){
        CHECK_EQUAL(s,"ERREUR: pixel hors de l'image.");
    }
}

TEST(GroupImage, getSetPixel5){
    Image testIm(256,256);
    testIm.setPixel(1,1,-10);
    testIm.setPixel(1,2,8000);
    CHECK_EQUAL(testIm.getPixel(1,1),0);
    CHECK_EQUAL(testIm.getPixel(1,2),255);
}

TEST(GroupImage, ecrirePNM){
    Image testIm(256,256);
    for(int i=0;i<testIm.getLargeur() || i<testIm.getHauteur();i++)
        testIm.setPixel(i,i,8000);
    try{
        ecrirePnm(testIm,"out/fichierSortie.pnm");
        CHECK(true);
    }
    catch(const std::string& s){
        FAIL("exception levee");
    }

}

TEST(GroupImage, remplir){
    Image testIm(512,512);
    remplir(testIm);
    ecrirePnm(testIm,"out/fichierSortieCosinus.pnm");

}

TEST(GroupImage, bordure){
    Image testIm(512,512);
    remplir(testIm);
    ecrirePnm(bordure(testIm,125,40),"out/fichierSortieBordure.pnm");
}
