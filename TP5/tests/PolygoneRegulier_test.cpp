#include "../src/PolygoneRegulier.hpp"
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupPolygoneRegulier){};

Couleur coul{1.0,0.5,0.3};
Point p{100,200};

PolygoneRegulier testPR(coul,p,50,5);
TEST(GroupPolygoneRegulier, test_getNbPts_PolygoneRegulier){
    CHECK_EQUAL(testPR.getNbPoints(),5);
}

TEST(GroupPolygoneRegulier, test_getPTS_PolygoneRegulier){
    CHECK_EQUAL(testPR.getPoint(1)._x,115);
    CHECK_EQUAL(testPR.getPoint(1)._y,247);
}
