#include "ViewerFigures.hpp"
#include "ZoneDessin.hpp"

ViewerFigures::ViewerFigures(int argc, char **argv) : _kit(argc,argv){
    _window.set_title("Titre");
    _window.set_default_size(640,480);
}

void ViewerFigures::run(){
    ZoneDessin dessin;
    _window.add(dessin);
    _window.show_all();
    _kit.run(_window);

}
