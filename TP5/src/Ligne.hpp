#ifndef LIGNE_
#define LIGNE_
#include "FigureGeometrique.hpp"
#include "Point.hpp"
#include <gtkmm.h>

class Ligne : public FigureGeometrique{
private:
    Point _p0;
    Point _p1;
public:
    Ligne(const Couleur& Couleur, const Point& p0, const Point& p1);
    void afficher() const override;
    void draw(const Cairo::RefPtr<Cairo::Context> &context) const override;
    const Point& getP0() const;
    const Point& getP1() const;
};

#endif
