#ifndef ZONEDESSIN_
#define ZONEDESSIN_

#include <gtkmm.h>
#include <vector>
#include "FigureGeometrique.hpp"

class ZoneDessin : public Gtk::DrawingArea{
    private:
        std::vector<FigureGeometrique*> _figures;
    public:
        ZoneDessin();
        ~ZoneDessin();
    protected:
        bool on_draw(const Cairo::RefPtr<Cairo::Context> & context) override;
        bool gererClic(GdkEventButton* event);
};

#endif
