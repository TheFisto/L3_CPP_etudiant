#include "Ligne.hpp"
#include <iostream>

Ligne::Ligne(const Couleur &Couleur, const Point &p0, const Point &p1) : FigureGeometrique(Couleur), _p0(p0), _p1(p1){}

void Ligne::afficher() const{
    std::cout<<"Ligne "<<_couleur._r<<"_"<<_couleur._g<<"_"<<this->getCouleur()._b
            <<" "<<_p0._x<<"_"<<_p0._y<<" "<<_p1._x<<"_"<<_p1._y<<std::endl;
}

const Point& Ligne::getP0() const{
    return _p0;
}

const Point& Ligne::getP1() const{
    return _p1;
}

void Ligne::draw(const Cairo::RefPtr<Cairo::Context> &context) const{
    //TODO
}
