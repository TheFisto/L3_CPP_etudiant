#ifndef FG_
#define FG_

#include "Couleur.hpp"
#include <vector>
#include <gtkmm.h>

class FigureGeometrique{
    protected:  //seul les classes héritant de celle ci on accès à ces attributs
        Couleur _couleur;
    public:
        //methodes
        FigureGeometrique(const Couleur& couleur);
        const Couleur& getCouleur() const;
        //methodes virtuelles
        virtual void afficher() const =0;
        virtual void draw(const Cairo::RefPtr<Cairo::Context> & context) const =0;
};

#endif
