#include "ZoneDessin.hpp"
#include "Ligne.hpp"
#include <iostream>

ZoneDessin::ZoneDessin(){
    _figures.push_back(new Ligne(Couleur{0.0,0.0,1.0},Point{0,0},Point{100,100}));
    _figures.push_back(new Ligne(Couleur{1.0,0.0,0.0},Point{0,100},Point{100,0}));
}

ZoneDessin::~ZoneDessin(){
    for(FigureGeometrique* fg : _figures)
        delete fg;
}

bool ZoneDessin::on_draw(const Cairo::RefPtr<Cairo::Context> & context){
    context->set_line_width(5.0);
    for(FigureGeometrique* fg: _figures){
        context->set_source_rgb(fg->getCouleur()._r,fg->getCouleur()._g,fg->getCouleur()._b);

    }
    context->stroke();
    return true;
}

bool ZoneDessin::gererClic(GdkEventButton *event){
    return true;
}
