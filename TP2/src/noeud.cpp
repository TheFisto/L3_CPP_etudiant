#include "noeud.h"

Noeud::Noeud(){
    _valeur=0;
    _suivant=nullptr;
}

Noeud::Noeud(int valeur){
    _valeur=valeur;
    _suivant=nullptr;
}

Noeud::~Noeud(){
    delete _suivant;
}
