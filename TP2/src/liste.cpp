#include "liste.h"

liste::liste(){
    _tete=nullptr;
}

void liste::ajouterDevant(int valeur){
    Noeud* nouveau = new Noeud(valeur);
    nouveau->_suivant=_tete;
    _tete = nouveau;
}

int liste::getTaille(){
    int total=0;
    Noeud* courant = _tete;
    while (courant) {
        courant=courant->_suivant;
        total++;
    }
    return total;
}

int liste::getElement(int indice){
    if(indice<0)
        return -1;
    Noeud* courant = _tete;
    for(int i=0;i<indice;i++){
        if(!courant)
            return -1;
        courant=courant->_suivant;
    }
    return courant->_valeur;
}

liste::~liste(){
    Noeud* precedent;
    while (_tete) {
        precedent=_tete;
        _tete=_tete->_suivant;
        delete precedent;
    }
}
