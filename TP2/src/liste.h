#ifndef LISTE_H
#define LISTE_H

#include "noeud.h"

class liste
{
public:
    Noeud* _tete;
    liste();
    ~liste();
    void ajouterDevant(int valeur);
    int getTaille();
    int getElement(int indice);
};

#endif // LISTE_H
