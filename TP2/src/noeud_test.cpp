#include "noeud.h"
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupNoeud){};
TEST(GroupNoeud, test_crea_noeud){
    Noeud n1;
    CHECK_EQUAL(n1._valeur, 0);
    CHECK(n1._suivant == nullptr);
    Noeud n2(4);
    CHECK(n2._suivant == nullptr);
    CHECK_EQUAL(n2._valeur,4);
}
