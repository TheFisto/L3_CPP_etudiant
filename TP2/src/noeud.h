#ifndef NOEUD_H
#define NOEUD_H


class Noeud
{
public:
    int _valeur;
    Noeud* _suivant;
    Noeud();
    Noeud(int valeur);
    ~Noeud();
};

#endif // NOEUD_H
