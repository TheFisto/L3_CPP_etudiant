#include "liste.h"
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupList){};
TEST(GroupList, test_crea_liste){
    liste l;
    CHECK(l._tete == nullptr);
}

TEST(GroupList, test_add_val){
    liste l;
    l.ajouterDevant(42);
    CHECK_EQUAL(l._tete->_valeur, 42);
    l.ajouterDevant(43);
    CHECK_EQUAL(l._tete->_valeur, 43);
    CHECK(l._tete->_suivant->_suivant == nullptr);
}

TEST(GroupList, test_get_taille){
    liste l;
    CHECK_EQUAL(l.getTaille(),0)
    l.ajouterDevant(42);
    l.ajouterDevant(42);
    l.ajouterDevant(42);
    l.ajouterDevant(42);
    l.ajouterDevant(42);
    CHECK_EQUAL(l.getTaille(),5);
}
 TEST(GroupList, test_get_element){
     liste l;
     l.ajouterDevant(44);
     l.ajouterDevant(43);
     l.ajouterDevant(42);
     l.ajouterDevant(41);
     l.ajouterDevant(40);
     CHECK_EQUAL(l.getElement(0),40);
     CHECK_EQUAL(l.getElement(4),44);
     CHECK_EQUAL(l.getElement(-4),-1);
     CHECK_EQUAL(l.getElement(800),-1);
 }
