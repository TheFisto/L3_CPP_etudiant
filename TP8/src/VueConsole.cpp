#include "VueConsole.hpp"
#include "Controleur.hpp"
#include <iostream>

VueConsole::VueConsole(Controleur & controleur) : Vue(controleur){}

void VueConsole::actualiser(){
    std::cout<<_controleur.getTexte()<<std::endl;
}

void VueConsole::run(){

}
