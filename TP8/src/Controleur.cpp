#include "Controleur.hpp"
#include "VueConsole.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

Controleur::Controleur(int argc, char ** argv) {
    _vues.push_back(std::make_unique<VueConsole>(*this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));
    for (auto & v : _vues)
      v->actualiser();
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}


std::string Controleur::getTexte(){
    std::stringstream ss;
    ss << _inventaire;
    return ss.str();
}

void Controleur::chargerInventaire(std::string nomFichier){
    std::ifstream ifs(nomFichier);
    Bouteille bout;
    while (ifs >> bout) {
        _inventaire._bouteilles.push_back(bout);
    }

    for(const std::unique_ptr<Vue> & v : _vues){
        v->actualiser();
    }
}
