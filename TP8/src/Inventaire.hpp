#ifndef INVENTAIRE_HPP_
#define INVENTAIRE_HPP_

#include "Bouteille.hpp"

#include <iostream>
#include <vector>
#include <ostream>

// Modèle : inventaire de bouteilles.
struct Inventaire {
    std::vector<Bouteille> _bouteilles;
};

std::ostream & operator<<(std::ostream & os, const Inventaire & inventaire);

#endif
