#include "Livre.hpp"

Livre::Livre() : _titre(""), _auteur(""), _annee(0){

}

Livre::Livre(const std::string &titre, const std::string auteur, int annee)
    : _titre(titre), _auteur(auteur), _annee(annee){
    std::string::size_type f = _titre.find(';');
    if(f != std::string::npos){
        throw std::string("erreur : titre non valide (';' non autorisé)");
    }
    f = _titre.find('\n');
    if(f != std::string::npos){
        throw std::string("erreur : titre non valide ('\n' non autorisé)");
    }
    f = _auteur.find(';');
    if(f != std::string::npos){
        throw std::string("erreur : auteur non valide (';' non autorisé)");
    }
    f = _auteur.find('\n');
    if(f != std::string::npos){
        throw std::string("erreur : auteur non valide ('\n' non autorisé)");
    }

}

const std::string Livre::getTitre() const { return _titre;}
const std::string Livre::getAuteur() const { return _auteur;}
int Livre::getAnnee() const { return _annee;}

bool Livre::operator<(const Livre & livre) const {
    if(_auteur != livre.getAuteur()){
        return _auteur < livre.getAuteur();
    }
    else{
        return _titre < livre.getTitre();
    }
}

bool operator==(const Livre & livre1, const Livre & livre2){
    return  livre1.getTitre()== livre2.getTitre()
            && livre1.getAuteur() == livre2.getAuteur()
            && livre1.getAnnee() == livre2.getAnnee();
}

std::ostream & operator<<(std::ostream & os, Livre & livre){
    os << livre.getTitre() << ';' << livre.getAuteur() << ';' << livre.getAnnee();
    return os;
}

std::istream & operator>>(std::istream & is, Livre & livre){
    std::string titre, auteur, annee;
    std::getline(is, titre, ';');
    std::getline(is, auteur, ';');
    std::getline(is, annee);
    if(titre!="" && auteur!="" && annee!="")
        livre = Livre(titre, auteur, std::stoi(annee));
    return is;
}
