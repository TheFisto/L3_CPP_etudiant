#include "Bibliotheque.hpp"
#include <iostream>
#include <fstream>
#include <algorithm>

void Bibliotheque::afficher() const{
    for(Livre l : *this){
        std::cout << l<<std::endl;
    }
}

void Bibliotheque::trierParAuteurEtTitre(){
    std::sort(this->begin(), this->end());
}

void Bibliotheque::trierParAnnee(){
    std::sort(this->begin(),this->end(),
        [](const Livre & l1, const Livre & l2){
            return l1.getAnnee()<l2.getAnnee();
        });
}


void Bibliotheque::lireFichier(const std::string &nomFichier){
    std::ifstream ifs(nomFichier);
    Livre l;
    while(ifs >> l){
        push_back(l);
    }
}

void Bibliotheque::ecrireFichier(const std::string &nomFichier){
    std::ofstream ofs(nomFichier);
    for(Livre l: *this){
        if(ofs.is_open()){
            ofs<<l<<"\n";
        }
    }
    ofs.close();
}
