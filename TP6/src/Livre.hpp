#ifndef LIVRE_
#define LIVRE_

#include "string"
#include <iostream>

class Livre{
private:
    std::string _titre,
                _auteur;
    int _annee;
public:
    //constructeurs
    Livre();
    Livre(const std::string & titre, const std::string auteur, int annee);
    //methodes
    const std::string getTitre() const;
    const std::string getAuteur() const;
    int getAnnee() const;
    //operateurs
    bool operator<(const Livre & livre) const ;
};
bool operator==(const Livre & livre1, const Livre & livre2);
std::ostream & operator<<(std::ostream & os, Livre & livre);
std::istream & operator>>(std::istream & is, Livre & livre);
#endif
