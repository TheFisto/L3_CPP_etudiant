#include "fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupFibo){};
TEST(GroupFibo, test_fibo_it){
	CHECK_EQUAL(0, fibonacciIteratif(0));
	CHECK_EQUAL(1, fibonacciIteratif(1));
	CHECK_EQUAL(1, fibonacciIteratif(2));
	CHECK_EQUAL(2, fibonacciIteratif(3));
	CHECK_EQUAL(3, fibonacciIteratif(4));
	CHECK_EQUAL(5, fibonacciIteratif(5));
	CHECK_EQUAL(8, fibonacciIteratif(6));
	CHECK_EQUAL(13, fibonacciIteratif(7));
}

TEST(GroupFibo, test_fibo_it_0){
	CHECK_EQUAL(0, fibonacciIteratif(0));
}

TEST(GroupFibo, test_fibo_it_1){
	CHECK_EQUAL(1, fibonacciIteratif(1));
}

TEST(GroupFibo, test_fibo_it_negatif){
	CHECK_EQUAL(-1, fibonacciIteratif(-2));
}

TEST(GroupFibo, test_fibo_rec){
	CHECK_EQUAL(0, fibonacciRecursif(0));
	CHECK_EQUAL(1, fibonacciRecursif(1));
	CHECK_EQUAL(1, fibonacciRecursif(2));
	CHECK_EQUAL(2, fibonacciRecursif(3));
	CHECK_EQUAL(3, fibonacciRecursif(4));
	CHECK_EQUAL(5, fibonacciRecursif(5));
	CHECK_EQUAL(8, fibonacciRecursif(6));
	CHECK_EQUAL(13, fibonacciRecursif(7));
}

TEST(GroupFibo, test_fibo_rec_0){
	CHECK_EQUAL(0, fibonacciRecursif(0));
}

TEST(GroupFibo, test_fibo_rec_1){
	CHECK_EQUAL(1, fibonacciRecursif(1));
}

TEST(GroupFibo, test_fibo_rec_negatif){
	CHECK_EQUAL(-1, fibonacciRecursif(-2));
}
