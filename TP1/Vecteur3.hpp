#ifndef _VECTEUR_
#define _VECTEUR_

struct Vecteur3{
	float x,y,z;
	
	void afficher();
	float norme();
	float produitScalaire(Vecteur3 vector);
	Vecteur3 addition(Vecteur3 vector);
};

#endif
