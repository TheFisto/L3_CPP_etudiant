#include <iostream>
#include <cmath>

struct Vecteur3{
	float x,y,z;
	
	void afficher();
	float norme();
	float produitScalaire(Vecteur3 vector);
	Vecteur3 addition(Vecteur3 vector);
};

void Vecteur3::afficher(){
	std::cout<<"("<<x<<","<<y<<","<<z<<")"<<std::endl;
}

float Vecteur3::norme(){
	return sqrt(pow(x,2)+pow(y,2)+pow(z,2));
}

float Vecteur3::produitScalaire(Vecteur3 vector){
	return norme()*vector.norme();
}

Vecteur3 Vecteur3::addition(Vecteur3 vector){
	Vecteur3 resultat {x+vector.x,y+vector.y,z+vector.z};
	return resultat;
}
