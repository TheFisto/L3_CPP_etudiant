#ifndef _FIBONACCI_
#define _FIBONACCI_

#include <iostream>

int fibonacciRecursif(int n){
	if(n<0)
		return -1;
	else if(n==0)
		return 0;
	else if(n==1)
		return 1;
	else
		return fibonacciRecursif(n-1)+fibonacciRecursif(n-2);
}

int fibonacciIteratif(int n){
	if(n<0)
		return -1;
	else if(n==0)
		return 0;
	else if(n==1)
		return 1;
		
	int n1=0, n2=1;
	for(int i=2;i<=n;i++){
		if(n1<n2)
			n1+=n2;
		else
			n2+=n1;
	}
	
	if(n1>n2)
		return n1;
	else
	return n2;
}

#endif
