#include <iostream>
#include "fibonacci.hpp"
#include "Vecteur3.hpp"
/*g++ -W -Wextra fibonacci.cpp main.cpp -o main */

int main(){
	std::cout<<"recursif : "<<fibonacciRecursif(7)<<"\niteratif : "<<fibonacciIteratif(7)<<std::endl;
	Vecteur3 vector1 {2,3,6};
	Vecteur3 vector2 {2,5,4};
	std::cout<<"Vecteur3d 1 et 2 : \n";
	vector1.afficher();
	vector2.afficher();
	std::cout<<"\nnormes : \n-1 : "<<vector1.norme()<<"\n-2 : "<<vector2.norme()<<std::endl;
	std::cout<<"Produit scalaire : "<<vector1.produitScalaire(vector2)<<std::endl;
	std::cout<<"Addition : ";
	vector1.addition(vector2).afficher();
	return 0;
}
