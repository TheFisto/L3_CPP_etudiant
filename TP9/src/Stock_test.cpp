#include "Stock.hpp"
#include <sstream>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupStock){};

TEST(GroupStock, TestStock1){
    Inventaire inventaire;
    inventaire._bouteilles.push_back(Bouteille{"cyanure", "2013-08-18", 0.25});
    inventaire._bouteilles.push_back(Bouteille{"mescaline", "2013-06-18", 0.1});
    Stock stock;
    stock.recalculerStock(inventaire);
    std::stringstream ss;
    ss << stock;
    CHECK_EQUAL(ss.str(),"cyanure;0.25\nmescaline;0.1\n")
}

TEST(GroupStock, TestStock2){
    Inventaire inventaire;
    inventaire._bouteilles.push_back(Bouteille{"cyanure", "2013-08-18", 0.25});
    inventaire._bouteilles.push_back(Bouteille{"cyanure", "2013-06-18", 0.1});
    Stock stock;
    stock.recalculerStock(inventaire);
    std::stringstream ss;
    ss << stock;
    CHECK_EQUAL(ss.str(),"cyanure;0.35\n")
}


TEST(GroupStock, TestStock3){
    Inventaire inventaire;
    inventaire._bouteilles.push_back(Bouteille{"cyanure", "2013-08-18", 0.25});
    inventaire._bouteilles.push_back(Bouteille{"mescaline", "2013-06-18", 0.1});
    inventaire._bouteilles.push_back(Bouteille{"cyanure", "2013-06-18", 0.1});
    Stock stock;
    stock.recalculerStock(inventaire);
    std::stringstream ss;
    ss << stock;
    CHECK_EQUAL(ss.str(),"cyanure;0.35\nmescaline;0.1\n")
}
