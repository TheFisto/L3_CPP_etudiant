#ifndef INVENTAIRE_HPP_
#define INVENTAIRE_HPP_

#include "Bouteille.hpp"

#include <iostream>
#include <vector>
#include <list>
#include <ostream>

// Modèle : inventaire de bouteilles.
struct Inventaire {
    std::list<Bouteille> _bouteilles;
    void trier();
};

std::ostream & operator<<(std::ostream & os, const Inventaire & inventaire);
std::istream & operator>>(std::istream & is, Inventaire & Inventaire);

#endif
