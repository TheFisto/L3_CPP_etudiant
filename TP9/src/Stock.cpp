#include "Stock.hpp"

#include <algorithm>

void Stock::recalculerStock(const Inventaire &inventaire){
    for(Bouteille b : inventaire._bouteilles){
        std::map<std::string,float>::iterator iter = _produits.find(b._nom);
        if(iter != _produits.end()){
            iter->second+=b._volume;
        }
        else{
            _produits.insert(std::pair<std::string,float>(b._nom,b._volume));
        }
    }
}

std::ostream & operator<<(std::ostream & os, const Stock & stock){
    for(std::pair<std::string,float> it : stock._produits){
        os << it.first << ";" << it.second << "\n";
    }
    return os;
}
