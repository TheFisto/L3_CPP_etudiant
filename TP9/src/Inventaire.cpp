#include "Inventaire.hpp"
#include <algorithm>

std::ostream & operator<<(std::ostream & os, const Inventaire & inventaire){

    std::for_each(inventaire._bouteilles.begin(),inventaire._bouteilles.end(),
        [& os](const Bouteille & b1){
            os << b1;
    });
    return os;
}



std::istream & operator>>(std::istream & is, Inventaire & inventaire){
    Bouteille bout;
    while(is >> bout){
        inventaire._bouteilles.push_back(bout);
    }
    return is;
}


void Inventaire::trier(){
    _bouteilles.sort([](const Bouteille & b1, const Bouteille & b2){
        return b1._nom<b2._nom;
    });
}
