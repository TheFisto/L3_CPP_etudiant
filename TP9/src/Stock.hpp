#ifndef STOCK_
#define STOCK_

#include <map>
#include <string>
#include <fstream>

#include "Inventaire.hpp"

class Stock{
private:
    std::map<std::string,float> _produits;
public:
    void recalculerStock(const Inventaire & inventaire);

    friend std::ostream & operator<<(std::ostream & os, const Stock & stock);
};



#endif
