#ifndef VUECONSOLE_
#define VUECONSOLE_

#include "Vue.hpp"

class VueConsole : public Vue{
public:
    VueConsole(Controleur & controleur);
    void actualiser();
    void run();
};

#endif
